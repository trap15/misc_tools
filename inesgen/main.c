/***************************************************************************\
*  inesgen                                                                  *
*  Code                                                                     *
* ------------------------------------------------------------------------- *
*  This code is copyright (C)2008 SquidMan (Alex Marshall)                  *
*  It is licensed under the GNU GPL v2. See LICENSE for more information.   *
\***************************************************************************/

/*
0	1	2	3	4	5	6	7	8	9	A	B	C	D	E	F
N	E	S	 	PRG	CHR	*	*2	\0	\0	\0	\0	\0	\0	\0	\0
4E	45	53	1A	PRG	CHR	*	*2	00	00	00	00	00	00	00	00
* Mapper & Mirror	*2 N/A Just use \0, 00
E.G. 0 1
0 = Mapper 0
1 = Vert Mirror

E.G.

0	1	2	3	4	5	6	7	8	9	A	B	C	D	E	F
N	E	S	 	 	 	\0	\0	\0	\0	\0	\0	\0	\0	\0	\0
4E	45	53	1A	01	01	00	00	00	00	00	00	00	00	00	00

Nes Game with a 16kb PRG and a 8kb CHR. Mapper 0, Horizontal Mirror Only
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
	char header[16];
	FILE* fp;
	if(argc < 6) {
		printf(	"Hai... You didn't use the correct syntax :)\n" \
				"The Correct syntax is:\n" \
				"%s <out.nes> <PrgSize> <ChrSize> <Mapper> <Mirror>\n\n" \
				"This program created by SquidMan (Alex Marshall)\n", argv[0]);
		exit(1);
	}
	memset(header, 0, 16);
	header[0] = 78;
	header[1] = 69;
	header[2] = 83;
	header[3] = 26;
	header[4] = atoi(argv[2]);					/* PRG Byte */
	header[5] = atoi(argv[3]);					/* CHR Byte */
	header[6] = (atoi(argv[4]) * 16) + atoi(argv[5]);		/* 3 is the Mapper Number, 4 is the Mirror Number */
	fp = fopen(argv[1], "wb+");
	if(fp == NULL) {
		printf("Can't open %s\n", argv[1]);
		exit(1);
	}
	fwrite(header, 16, 1, fp);
	fclose(fp);

	return 0;
}
