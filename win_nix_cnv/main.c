/***************************************************************************\
*  win_nix_cnv                                                              *
*  Code                                                                     *
* ------------------------------------------------------------------------- *
*  This code is copyright (C)2008 SquidMan (Alex Marshall)                  *
*  It is licensed under the GNU GPL v2. See LICENSE for more information.   *
\***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void bad_args(int argc, char *argv[])
{
	printf("BAD ARGUMENTS!\nCorrect calling is:\n\t%s <in.txt> [out.txt]\n", argv[0]);
	exit(1);
}

int main(int argc, char *argv[])
{
	FILE* infp;
	int infilesize;
	unsigned char* infile;
	int i;
	int win_cnt;
	int outfilesize;
	unsigned char* outfile;
	int z;
	FILE* outfp;
	printf("Win Nix Text Converter. (c) 2008 SquidMan (Alex Marshall)\n");
	if(argc != 2 && argc != 3) {
		bad_args(argc, argv);
	}
	infp = fopen(argv[1], "rb");
	if(infp == NULL)
		bad_args(argc, argv);

	fseek(infp, 0, SEEK_END);
	infilesize = ftell(infp);
	infile = malloc(infilesize);
	fseek(infp, 0, SEEK_SET);
	fread(infile, infilesize, 1, infp);
	win_cnt = 0;
	for(i = 0; i < infilesize; i++) {
		if(infile[i] == '\x0d' && infile[i+1] == '\x0a')
			win_cnt++;
	}
	outfilesize = infilesize - win_cnt;
	outfile = malloc(outfilesize);
	for(i = 0, z = 0; i < infilesize; i++, z++) {
		if(infile[i] == '\x0d' && infile[i+1] == '\x0a') {
			outfile[z] = '\x0a';
			i++;
		}else
			outfile[z] = infile[i];
	}
	fclose(infp);
	free(infile);
	if(argc == 3) {
		outfp = fopen(argv[2], "wb+");
		if(outfp == NULL)
			bad_args(argc, argv);
	}else{
		outfp = fopen(argv[1], "wb+");
		if(outfp == NULL)
			bad_args(argc, argv);
	}
	fwrite(outfile, outfilesize, 1, outfp);
	fclose(outfp);
	free(outfile);
	return 0;
}
