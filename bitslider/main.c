/***************************************************************************\
 *  bitslider                                                                *
 *  Code                                                                     *
 * ------------------------------------------------------------------------- *
 *  This code is copyright (C)2009 SquidMan (Alex Marshall)                  *
 *  It is licensed under the GNU GPL v2. See LICENSE for more information.   *
 \***************************************************************************/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	FILE* fp;
	int size;
	char *buf;
	int i;
	printf("bitslider (c)2009 SquidMan (Alex Marshall) Licensed under GNU GPL v2\n");
	if(argc < 4) {
		printf("Invalid arguments. Usage:\n\t%s <in.bin> <out.bin> <value> [skip]\n", argv[0]);
		return 1;
	}
	fp = fopen(argv[1], "rb");
	if(fp == NULL) {
		printf("Unable to open %s for reading.\n", argv[1]);
		return 1;
	}
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	buf = calloc(size, 1);
	if(buf == NULL) {
		printf("Unable to allocate buffer.\n");
		return 1;
	}
	fread(buf, 1, size, fp);
	fclose(fp);
	for(i = 0; i < size; i++) {
		buf[i] += atoi(argv[3]);
		if(argc > 4)
			i += atoi(argv[4]);
	}
	fp = fopen(argv[2], "wb");
	if(fp == NULL) {
		printf("Unable to open %s for writing.\n", argv[2]);
		return 1;
	}
	fwrite(buf, 1, size, fp);
	fclose(fp);
	free(buf);
	return 0;
}
