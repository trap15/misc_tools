/***************************************************************************\
*  iplsaveparser                                                            *
*  Main                                                                     *
* ------------------------------------------------------------------------- *
*  This code is copyright (C)2009 SquidMan (Alex Marshall)                  *
*  It is licensed under the GNU GPL v2. See LICENSE for more information.   *
\***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"

static size_t fileoffset = 0;

#define fatal(x,y)		printf(x,y); exit(1)
#define PAGESTYLE		1
#define TITLEIDBREAK(x)		(((x<<32)>>32)>>24), (((x<<40)>>40)>>16), (((x<<48)>>48)>>8), (((x<<56)>>56)>>0)

u16 be16(u16 x)
{
	return (x>>8) | 
        (x<<8);
}

u32 be32(u32 x)
{
	return (x>>24) | 
        ((x<<8) & 0x00FF0000) |
        ((x>>8) & 0x0000FF00) |
        (x<<24);
}

u64 be64(u64 x)
{
	return (x>>56) | 
        ((x<<40) & (u64)0x00FF000000000000LL) |
        ((x<<24) & (u64)0x0000FF0000000000LL) |
        ((x<<8)  & (u64)0x000000FF00000000LL) |
        ((x>>8)  & (u64)0x00000000FF000000LL) |
        ((x>>24) & (u64)0x0000000000FF0000LL) |
        ((x>>40) & (u64)0x000000000000FF00LL) |
        (x<<56);
}


static void ReadDataFromMemory(void* destination, void* input, size_t size)
{
	u8* out = (u8*)destination;
	u8* in = ((u8*)input) + fileoffset;
	memcpy(out, in, size);
	fileoffset += size;
}


void GetIplSaveListing(u8* iplfile, int channelcount, iplsave_entry* entries)
{
	int i;
	for(i = 0; i < channelcount; i++) {
		ReadDataFromMemory(&(entries[i]), iplfile, sizeof(iplsave_entry));
	}
}

int main (int argc, const char * argv[])
{
	if(argc != 2) {
		printf("Wrong arguments!\n\t%s <iplsave.bin>\n", argv[0]);
		exit(1);
	}
	FILE* fp = fopen(argv[1], "rb");
	if(fp == NULL) {
		fatal("Unable to open fp %s!\n", argv[1]);
	}
	fseek(fp, 0, SEEK_END);
	int filesize = ftell(fp);
	u8* iplfile = (u8*)calloc(filesize, 1);
	fseek(fp, 0, SEEK_SET);
	fread(iplfile, filesize, 1, fp);
	iplsave base;
	ReadDataFromMemory(&(base.magic), iplfile, 4);
	ReadDataFromMemory(&(base.filesize), iplfile, 4);
	ReadDataFromMemory(base.unknown, iplfile, 8);
	if(strcmp(base.magic, "RIPL") != 0) {
		fatal("Invalid magic '%s'! Should be 'RIPL'!\n", base.magic);
	}
	if(be32(base.filesize) != filesize) {
		fatal("Filesize is not correct %d!\n", be32(base.filesize));
	}
	int channelcount = 0x30;
	iplsave_entry *entries = (iplsave_entry*)calloc(sizeof(iplsave_entry), channelcount);
	char chanlst[0x30][8];
	GetIplSaveListing(iplfile, channelcount, entries);
	int i;
	int brackettype;
	int endtype;
	int lines = 0;
	char append[8];
	for(i = 0; i < channelcount; i++) {
		brackettype = 0;
		endtype = 0;
		if(be16(entries[i].moveable) == 0x000f)
			brackettype = 1;
		if((i % 4) == 3) {
			endtype = 1;
			lines++;
		}
		u64 x = be64(entries[i].title_id);
		sprintf(chanlst[i], "%c", brackettype ? '<' : '[');
		if(entries[i].channel_type == 03)
			sprintf(append, "%c%c%c%c", (u8)(((x<<32)>>32)>>24), (u8)(((x<<40)>>40)>>16), (u8)(((x<<48)>>48)>>8), (u8)(((x<<56)>>56)>>0));
		else if(entries[i].channel_type == 01)
			sprintf(append, "DISC");
		else
			sprintf(append, "    ");
		strcat(chanlst[i], append);
		sprintf(append, "%c%c", brackettype ? '>' : ']', ' ');
		strcat(chanlst[i], append);
	}
	int page, line, item;
#if PAGESTYLE == 0
	for(line = 0; line < 3; line++) {
		for(page = 0; page < 4; page++) {
			for(item = 0; item < 4; item++) {
				printf("%s", chanlst[((line + (page * 3)) * 4) + item]);
			}
			printf(" ");
		}
		printf("\n");
	}
#else
	for(page = 0; page < 4; page++) {
		for(line = 0; line < 3; line++) {
			for(item = 0; item < 4; item++) {
				printf("%s", chanlst[((line + (page * 3)) * 4) + item]);
			}
			printf("\n");
		}
		printf("\n");
	}	
#endif
}
