/***************************************************************************\
*  tiktmdstrip                                                              *
*  Code                                                                     *
* ------------------------------------------------------------------------- *
*  This code is copyright (C)2009 SquidMan (Alex Marshall)                  *
*  It is licensed under the GNU GPL v2. See LICENSE for more information.   *
\***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

void strip_tmd(const char* path)
{
	FILE* fp;
	short cnt;
	unsigned char* tmdbuf;
	cnt = 0;
	fp = fopen(path, "rb");
	fseek(fp, 0x1de, SEEK_SET);
	fread(&cnt, 2, 1, fp);
	cnt = (cnt >> 8) + (cnt << 8);
	fseek(fp, 0, SEEK_SET);
	tmdbuf = malloc(0x1e4 + (0x24 * cnt));
	fread(tmdbuf, 0x1e4 + (0x24 * cnt), 1, fp);
	fclose(fp);
	fp = fopen(path, "wb");
	fwrite(tmdbuf, 0x1e4 + (0x24 * cnt), 1, fp);
	fclose(fp);
}

void strip_tik(const char* path)
{
	FILE* fp;
	unsigned char* tikbuf;
	fp = fopen(path, "rb");
	fseek(fp, 0, SEEK_SET);
	tikbuf = malloc(676);
	fread(tikbuf, 676, 1, fp);
	fclose(fp);
	fp = fopen(path, "wb");
	fwrite(tikbuf, 676, 1, fp);
	fclose(fp);
}

int main(int argc, char *argv[])
{
	char* typestr;
	int i;
	int typestrlen;
	if(argc != 3) {
		printf("Bad args: %s [tmd|tik] <file>\n", argv[0]);
		exit(1);
	}
	typestr = (char*)malloc(strlen(argv[1]));
	typestrlen = strlen(argv[1]);
	for(i = 0; i < typestrlen; typestr[i] = toupper(argv[1][i]), i++);
	if(strcmp(typestr, "TMD") == 0)
		strip_tmd(argv[2]);
	else if(strcmp(typestr, "TIK") == 0)
		strip_tik(argv[2]);
	return 0;
}
