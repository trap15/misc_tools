/***************************************************************************\
*  ecc_strip                                                                *
*  Code                                                                     *
* ------------------------------------------------------------------------- *
*  This code is copyright (C)2009 SquidMan (Alex Marshall)                  *
*  It is licensed under the MIT License.                                    *
\***************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#define PAGE_SIZE	(2048)
#define SPARE_SIZE	(64)
#define PAGE_COUNT	(262144)

int main(int argc, char *argv[])
{
	FILE* infp;
	FILE* outfp;
	unsigned char buf[PAGE_SIZE];
	int i;
	printf("ecc_strip (c)2009~2011 trap15 (Alex Marshall) Licensed under MIT License\n");
	if(argc != 3) {
		printf("Invalid arguments. Usage:\n\t%s <input.img> <output.img>\n", argv[0]);
		return 1;
	}
	infp = fopen(argv[1], "rb");
	outfp = fopen(argv[2], "wb");
	for(i = 0; i < PAGE_COUNT; i++) {
		fread(buf, PAGE_SIZE, 1, infp);
		fseek(infp, SPARE_SIZE, SEEK_CUR);
		fwrite(buf, PAGE_SIZE, 1, outfp);
	}
	return 0;
}
